<?php

namespace App\Console\Commands;

use App\Models\Company;
use App\Services\GetCompanyHolidays;
use App\Services\GetGoogleHolidays;
use App\Services\GetYourWorkingDays;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Routing\Exception\InvalidArgumentException;

class GetYourWorkingDaysCommand extends Command
{

    public $rules = [
        'workingHoursPerDay' => 'required|array',
        'workingHoursPerDay.*' => 'integer',
        'company' => 'required|numeric'
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kingscode:get-working-days
                            {workingHoursPerDay* : Add you days to work as an array like 8 8 8 4}
                            {--year=* : Add years as array or use it like 2022-2030}
                            {--C|company= : Use google as your holiday standard}
                            {--G|google : Use google as your holiday standard}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get the days in the week for you partime job if you want ot work least.';

    private GetGoogleHolidays $getGoogleHolidays;
    private GetCompanyHolidays $getCompanyHolidays;

    private GetYourWorkingDays $getYourWorkingDays;

    /**
     * @param GetGoogleHolidays $getGoogleHolidays
     */
    public function __construct(
        GetGoogleHolidays  $getGoogleHolidays,
        GetYourWorkingDays $getYourWorkingDays,
        GetCompanyHolidays $getCompanyHolidays
    )
    {
        parent::__construct();
        $this->getGoogleHolidays = $getGoogleHolidays;
        $this->getCompanyHolidays = $getCompanyHolidays;
        $this->getYourWorkingDays = $getYourWorkingDays;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->validate()) {
            $this->handleAfterValidation();
        }
    }

    public function validate()
    {
        $validateAll = array_merge($this->arguments(), $this->option());
        $validator = Validator::make($validateAll, $this->rules);
        if (!$validator->passes()) {
            $this->error($validator->messages());
            return false;
        }

        return true;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handleAfterValidation()
    {

        $this->output->title('KingsCode - Challenge to work as little as possible.');

        if (empty($this->option('year'))) {
            $years = [Carbon::now()->year];
        } else {
            $years = [];
            foreach ($this->option('year') as $year) {
                if (strpos($year, '-') !== false) {
                    $startYear = (int)trim(explode('-', $year)[0]);
                    $endYear = (int)trim(explode('-', $year)[1]);
                    if ($startYear > $endYear) {
                        throw new InvalidArgumentException('End year can not be before the start year');
                    }
                    for ($runningYear = $startYear; $runningYear <= $endYear; $runningYear++) {
                        $years[$runningYear] = $runningYear;
                    }
                } else {
                    $years[$year] = $year;
                }
            }
            sort($years);
            $years = array_unique($years, SORT_NUMERIC);
        }

        $requestedWorkingHoursPerDay = $this->argument('workingHoursPerDay');

        foreach ($years as $year) {
            if (!empty($this->option('google'))) {
                $holidays = $this->getGoogleHolidays->serve($year);
                if (count($holidays) <= 0) {
                    $this->output->error('Currently there are no national holidays registered at the Google Api for ' . $year);
                }
            } else {
                $companyId = $this->option('company');
                $holidays = $this->getCompanyHolidays->serve($companyId, $year);
                if (count($holidays) <= 0) {
                    $company = Company::findOrFail($companyId);
                    $this->output->error(sprintf('Currently there are no national holidays registered for %s @ %s (%s)',$year, $company->name, $companyId));
                }
            }

            $yourWorkingDays = $this->getYourWorkingDays->serve($requestedWorkingHoursPerDay, $holidays);
            $this->displayWorkingDaysPretty($year, $yourWorkingDays, $holidays);
        }

        return 0;
    }

    private function displayWorkingDaysPretty($year, $yourWorkingDays, $holidays)
    {
        if (count($holidays) > 0) {
            $this->output->title(sprintf('Your working days for  %s', $year));
            $this->output->section(sprintf('Public holidays (%d) for %d', count($holidays), $year));

            $yourWorkingDaysArr = $yourWorkingDays->resource;

            $this->output->table(['Name', 'Start Date', 'End Date', 'Day of the week'], $holidays);

            $this->output->section('Results according to google.');
            if ($yourWorkingDaysArr['totalWorkingHoursAWeek'] > 40) {
                $this->output->warning('It is irresponsible of you to work more than 40 hours a week.');
            }
            if ($yourWorkingDaysArr['daysOfWorkAWeek'] > 5) {
                $this->output->warning('It is irresponsible of you to work more than 5 days a week, take a break...really');
            }
            $this->output->writeln(sprintf('According to your input you would like to work %s hours a week', $yourWorkingDaysArr['totalWorkingHoursAWeek']));
            $this->output->writeln(sprintf('In a total of %s days', $yourWorkingDaysArr['daysOfWorkAWeek']));
            $this->output->newLine();
            $this->output->writeln('According to our calculations you will be working least on.');
            $this->output->table(['Day of the week', 'Number of times'],
                $this->convertAssociativeArrayToTableResult($yourWorkingDaysArr['daysToWorkAWeek']));
            $this->output->newLine();
            $this->output->writeln('Some fun facts:');
            $this->output->writeln(sprintf('There are in total %s week days in %s', $yourWorkingDaysArr['weekDaysInTheYear'], $year));
            $this->output->writeln(sprintf('There are in total %s weekend days in %s', $yourWorkingDaysArr['weekendDaysInTheYear'], $year));
            $this->output->writeln(sprintf('You will be working a total of %s hours', $yourWorkingDaysArr['totalWorkingHours']));

            $this->output->table(['Day of the week', 'Total hours'],
                $this->convertAssociativeArrayToTableResult($yourWorkingDaysArr['totalWorkingHoursPerDay']));

        }
    }

    private function convertAssociativeArrayToTableResult(array $data)
    {
        return array_map(fn(string $k, string $v) => [$k, $v], array_keys($data), array_values($data));
    }
}
