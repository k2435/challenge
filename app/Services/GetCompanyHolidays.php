<?php

namespace App\Services;

use App\Models\Company;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class GetCompanyHolidays
{

    public function serve($companyId, $year)
    {
        $holidays = Company::findOrFail($companyId)->holidays;
        $result = [];
        foreach ($holidays as $holiday) {
            if (
                isset($holiday->startDate)
                && (explode('-', $holiday->startDate)[0] == $year)
                && $holiday->description
            ) {
                $result[] = [
                    $holiday->description,
                    $holiday->startDate,
                    $holiday->endDate,
                    Carbon::parse($holiday->startDate)->dayName
                ];
            }
        }
        if (!empty($result)) {
            usort($result, fn($x, $y) => $x[1] <=> $y[1]);
        }
        return $result;
    }
}
