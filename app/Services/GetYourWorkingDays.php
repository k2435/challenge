<?php

namespace App\Services;

use App\Http\Resources\WorkingDaysResultResource;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class GetYourWorkingDays
{

    /**
     * @param array $requestedWorkingHoursPerDay
     * @param array $holidays
     * @return WorkingDaysResultResource|null
     */
    public function serve(array $requestedWorkingHoursPerDay, array $holidays)
    {
        if(!empty($requestedWorkingHoursPerDay) && !empty($holidays)){
            $year = explode('-', $holidays[0][1])[0];

            $holidaysInCarbon = array_map(fn($holiday) => Carbon::createFromFormat('Y-m-d', $holiday[1]), $holidays);
            $counterResult = [0,0,0,0,0,0,0];

            $daysInTheYear = Carbon::parse($year)->daysInYear;
            $workingDaysInTheYear = 0;
            $weekendDaysInTheYear = 0;
            $dateRange = CarbonPeriod::create(
                Carbon::create($year, 1, 1, 0, 0, 0),
                Carbon::create($year, 12, 31, 0, 0, 0)
            );
            foreach ($dateRange->toArray() as $carbon){
                if($carbon->isWeekday()){
                    $workingDaysInTheYear ++;
                    if(count(array_filter($holidaysInCarbon, function($holiday) use ($carbon) {
                            return $holiday->isSameDay($carbon);
                        })) <= 0 ){
                        $counterResult[$carbon->dayOfWeek] ++;
                    }
                } else {
                    $weekendDaysInTheYear ++;
                }
            }

            $counterResult = array_combine(Carbon::getDays(), $counterResult);

            $filteredDaysOfWork = array_filter($requestedWorkingHoursPerDay, fn($dayOfWork) => ($dayOfWork > 0));
            $daysOfWorkAWeek = count($filteredDaysOfWork);
            $totalWorkingHoursAWeek = array_sum($filteredDaysOfWork);

            asort($counterResult);

            $counterResult = array_filter($counterResult);
            $workingDaysMapped =  array_slice($counterResult, 0, $daysOfWorkAWeek);

            rsort($filteredDaysOfWork);

            $i = 0;
            foreach ($workingDaysMapped as $day => $daysCount){
                $totalWorkingHoursPerDay[$day] = $filteredDaysOfWork[$i] * $daysCount;
                $i++;
            }
            $totalWorkingHours = array_sum($totalWorkingHoursPerDay);

            return new WorkingDaysResultResource([
                'totalWorkingHours' => $totalWorkingHours,
                'totalWorkingHoursPerDay' => $totalWorkingHoursPerDay,
                'totalWorkingHoursAWeek' => $totalWorkingHoursAWeek,
                'weekDaysInTheYear' => $workingDaysInTheYear,
                'weekendDaysInTheYear' => $weekendDaysInTheYear,
                'daysInTheYear' => $daysInTheYear,
                'daysOfWorkAWeek' => $daysOfWorkAWeek,
                'daysToWorkAWeek' => $workingDaysMapped,
                'filteredDaysOfWork' => $filteredDaysOfWork,
                'counterResult' => $counterResult,
                'theHolidays' =>$holidays
            ]);
        }

        return new WorkingDaysResultResource([
            'success' => false,
            'message' => 'We do not have enough data'
        ]);
    }
}
