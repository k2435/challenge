<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class GetGoogleHolidays
{

    private const BASE_CALENDAR_URL = "https://www.googleapis.com/calendar/v3/calendars";
    private const BASE_CALENDAR_ID_FOR_PUBLIC_HOLIDAY = "holiday@group.v.calendar.google.com";
    private $apiKey;
    private const DEFAULT_CALENDAR_REGION = "en.dutch";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->apiKey = getenv('GOOGLE_API');
    }

    public function serve($year, $calendarRegion = self::DEFAULT_CALENDAR_REGION){
        $url = sprintf('%s/%s%%23%s/events?key=%s', self::BASE_CALENDAR_URL, $calendarRegion, self::BASE_CALENDAR_ID_FOR_PUBLIC_HOLIDAY, $this->apiKey);
        $result = [];
        try {
            $response = Http::get($url);
            if($response->failed()){
                throw new \Exception($response->reason());
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            throw $e;
        }

        if($response && !$response->failed()){
            foreach ($response->json('items') as $item) {
                if(
                    isset($item['start'])
                    && isset($item['start']['date'])
                    && (explode('-', $item['start']['date'])[0] == $year)
                    && $item['status'] === 'confirmed'
                    && $item['description'] === 'Public holiday'
                    && $item['visibility'] === 'public'
                ) {
                    $result[] = [
                        $item['summary'],
                        $item['start']['date'],
                        $item['end']['date'],
                        Carbon::parse($item['start']['date'])->dayName
                    ];
                }
            }
        }
        usort($result, fn($x, $y) => $x[1] <=> $y[1]);
        return $result;
    }
}
