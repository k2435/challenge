<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Holidays extends Model
{
    use HasFactory;

    protected $table = 'holidays';
    protected $connection = 'sqlite';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'description',
        'startDate',
        'endDate',
    ];


    public function companies()
    {
        return $this->belongsToMany(Company::class, 'company_holidays');
    }
}
