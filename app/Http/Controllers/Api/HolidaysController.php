<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyHolidaysResource;
use App\Http\Resources\CompanyResource;
use App\Models\Company;
use App\Models\Holidays;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class HolidaysController extends Controller
{
    /**
     * @param $holidayId
     * @return JsonResponse
     */
    public function destroy($holidayId)
    {
        $holiday = Holidays::findOrFail($holidayId);
        return response()->json(['success' => $holiday->deleteOrFail()]);
    }
}
