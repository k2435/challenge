<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyHolidaysResource;
use App\Http\Resources\CompanyResource;
use App\Models\Company;
use App\Models\Holidays;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class CompanyHolidaysController extends Controller
{
    /**
     * @param $companyId
     * @param int $year
     * @return JsonResponse
     * @throws ValidationException
     */
    public function index($companyId, $year = 0)
    {
        $validator = Validator::make(['year' => $year], [
            'year' => 'date_format:Y',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $company = Company::findOrFail($companyId);
        $validatedData = $validator->validated();

        if(isset($validatedData['year']) && $validatedData['year'] > 0){
            $holidays = array_filter($company->holidays->toArray(), fn($holiday) => ($holiday && explode('-', $holiday['startDate'])[0] == $year) ? $holiday : null);
        } else {
            $holidays = $company->holidays;
        }

        return response()->json(CompanyHolidaysResource::collection($holidays));
    }

    /**
     * @param Request $request
     * @param int $companyId
     * @return JsonResponse|object
     * @throws ValidationException
     */
    public function store(Request $request, int $companyId)
    {
        $validator = Validator::make($request->all(), [
            'holidays' => 'array',
            'description.*' => 'required|max:100',
            'startDate.*' =>  'required|date_format:Y-m-d',
            'endDate.*' =>  'date_format:Y-m-d|after_or_equal:startDate',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $company = Company::findOrFail($companyId);

        $validatedData = $validator->validated();
        $createdHolidays = [];

        foreach ($validatedData['holidays'] as $holidayData){
            if(!isset($holidayData['endDate'])){
                $holidayData['endDate'] =
                    Carbon::createFromFormat('Y-m-d', $holidayData['startDate'])
                        ->addDay()
                        ->format('Y-m-d');
            }
            $holiday = Holidays::create($holidayData);
            $holiday->save();
            $createdHolidays[] = $holiday->id;
        }

        $company->holidays()->attach($createdHolidays);

        return response()->json(CompanyHolidaysResource::collection($company->holidays))->setStatusCode(201);
    }

    /**
     * @param Request $request
     * @param $companyId
     * @param $year
     * @return JsonResponse
     * @throws ValidationException
     */
    public function destroy(Request $request, $companyId, $year)
    {
        $validator = Validator::make(['year' => $year], [
            'year' => 'required|date_format:Y',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $company = Company::findOrFail($companyId);
        $validatedData = $validator->validated();

        foreach($company->holidays as $holiday){
            $carbonDate = Carbon::createFromFormat('Y-m-d', $holiday->startDate);
            if($carbonDate->isSameYear($validatedData['year'])) {
               $result = $holiday->deleteOrFail();
            }
        }

        return response()->json(['success' => $result]);
    }
}
