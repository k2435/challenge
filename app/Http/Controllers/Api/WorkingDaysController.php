<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyHolidaysResource;
use App\Http\Resources\WorkingDaysResultResource;
use App\Models\Company;
use App\Models\Holidays;
use App\Services\GetCompanyHolidays;
use App\Services\GetGoogleHolidays;
use App\Services\GetYourWorkingDays;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\Routing\Exception\InvalidArgumentException;

class WorkingDaysController extends Controller
{
    /**
     * @param Request $request
     * @param $companyId
     * @param GetGoogleHolidays $getGoogleHolidays
     * @param GetCompanyHolidays $getCompanyHolidays
     * @param GetYourWorkingDays $getYourWorkingDays
     * @return JsonResponse
     * @throws ValidationException
     */
    public function index(
        Request $request,
        $company,
        GetGoogleHolidays $getGoogleHolidays,
        GetCompanyHolidays $getCompanyHolidays,
        GetYourWorkingDays $getYourWorkingDays
    )
    {

        $validator = Validator::make($request->all(), [
            'workingHoursPerDay' => 'required|array',
            'workingHoursPerDay.*' => 'integer',
            'year' => 'date_format:Y',
            'google' => 'boolean',
            'simple' => 'boolean',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $validatedData = $validator->validated();

        if (!isset($validatedData['year'])) {
            $year = Carbon::now()->year;
        } else {
            $year = $validatedData['year'];
        }
        if(isset($validatedData['google']) && $validatedData['google']){
            try {
                $holidays = $getGoogleHolidays->serve($year);
            } catch (\Exception $e) {
                return response()->json([
                    "success"  => false,
                    "message" => 'Are you missing the correct ApiKey'
                ], 500);
            }
        } else {
            $holidays = $getCompanyHolidays->serve($company, $year);
        }

        if(empty($holidays)){
            return response()->json([
                "success"  => false,
                "message" => "No holidays are registered for " . $year
            ], 404);
        }

        $result = $getYourWorkingDays->serve($validatedData['workingHoursPerDay'], $holidays);

        if(empty($result)){
            return response()->json([
                "success"  => false,
                "message" => "No results found"
            ], 404);
        }
        if(isset($validatedData['simple'])){
            return response()->json($result['daysToWorkAWeek']);
        }
        return response()->json($result);
    }
}
