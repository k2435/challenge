<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyHolidaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_holidays', function (Blueprint $table) {
                $table->integer('company_id')->unsigned()->index();
                $table->foreign('company_id')->references('id')
                    ->on('companies')
                    ->onDelete('cascade');
                $table->integer('holidays_id')->unsigned()->index();
                $table->foreign('holidays_id')->references('id')
                    ->on('holidays')
                    ->onDelete('cascade');
                $table->primary(['company_id', 'holidays_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_holidays');
    }
}
