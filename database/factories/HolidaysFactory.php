<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class HolidaysFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        dd($this->states->toArray());
        return [
            'description' => $desc,
            'startDate' => $start->format('Y-m-d'),
            'endDate' => $start->addDay()->format('Y-m-d')
        ];
    }
}
