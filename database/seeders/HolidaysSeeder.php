<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Holidays;
use App\Models\Wolf;
use App\Models\WolfPack;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class HolidaysSeeder extends Seeder
{

    private $holidays=[
        'Nieuwjaar' => '2022-01-01',
        'Goed vrijdag' => '2022-04-15',
        'Eerste passdag' => '2022-04-17',
        'tweede paasdag' => '2022-04-18',
        'Koningsdag' => '2022-04-27',
        'Bevrijdingsdag' => '2022-05-05',
        'Hemelvaart' => '2022-05-26',
        'Eerste pinksterdag' => '2022-06-05',
        'Tweede pinksterdag' => '2022-06-06',
        'Kerst' => '2022-12-25',
        'Tweede Kerstdag' => '2022-12-26',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach (range(0, 5) as $iteration)
        {
            foreach($this->holidays as $desc => $date){
                $start = Carbon::createFromFormat('Y-m-d', $date);
                $start->addYears($iteration);
                $holiday = new Holidays([
                    'description' => $desc,
                    'startDate' => $start->format('Y-m-d'),
                    'endDate' => $start->addDay()->format('Y-m-d')
                ]);
                $holiday->saveQuietly();
            }
        }

        $holidays = Holidays::all();
        Company::all()->each(function($company) use ($holidays) {
            $randHolidays = random_int(1, count($holidays));
            $randomHolidays = $holidays->take($randHolidays)->pluck('id');
            $company->holidays()->sync($randomHolidays);
        });
    }
}
