<?php

namespace Tests\Feature;

use App\Models\Company;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HolidaysTest extends TestCase
{
    /**
     *
     */
    public function test_company_holidays()
    {
        // Create
        $found = Company::firstOrNew(
                    ['name' => 'KingsCode Test']
                );

        $companyId = $found->id;

        fwrite(STDOUT, 'got Company' . PHP_EOL);

        $response = $this->postJson('/api/company/' . $companyId . '/holidays', ['holidays' => [
            [
                "description" => "test",
                "startDate" => "2022-01-01"
            ],
            [
                "description" => "test2",
                "startDate" => "2022-02-01"
            ],
        ]
        ]);
        $response
            ->assertStatus(201)
            ->assertJsonFragment(
                [
                    "description" => "test"
                ]
            )
            ->assertJsonFragment(
                [
                    "description" => "test2"
                ]
            );


        fwrite(STDOUT, 'Created Holidays' . PHP_EOL);

        //Delete
        foreach ($response->json() as $holidays) {
            $response = $this->deleteJson('/api/holiday/' . $holidays['id']);
            $response
                ->assertStatus(200)
                ->assertJson([
                    'success' => 1
                ]);

            fwrite(STDOUT, 'Deleted holiday ' . $holidays['description'] . PHP_EOL);
        }

    }
}
