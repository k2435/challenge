<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CompanyTest extends TestCase
{
    /**
     * @return void
     */
    public function test_company()
    {
        // Create
        $response = $this->postJson('/api/company', ['name' => 'KingsCode Test']);
        $response
            ->assertStatus(201)
            ->assertJsonFragment([
                'name' => 'KingsCode Test',
            ]);
        $companyId = $response->json('id');

        fwrite(STDOUT,'Created Company' . PHP_EOL);
        //Get
        $response = $this->getJson('/api/company/' . $companyId);
        $response
            ->assertStatus(200)
            ->assertJson([
                'name' => 'KingsCode Test',
            ]);

        fwrite(STDOUT,'Get Company' . PHP_EOL);

        //Update
        $response = $this->putJson('/api/company/' . $companyId, ['name' => 'KingsCode Test Updated']);
        $response
            ->assertStatus(201)
            ->assertJson([
                'name' => 'KingsCode Test Updated',
            ]);

        fwrite(STDOUT,'Updated Company' . PHP_EOL);

        //Delete
        $response = $this->deleteJson('/api/company/' . $companyId);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => 1
            ]);

        fwrite(STDOUT,'Deleted Company' . PHP_EOL);
    }
}
