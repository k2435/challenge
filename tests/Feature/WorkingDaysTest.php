<?php

namespace Tests\Feature;

use App\Models\Company;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class WorkingDaysTest extends TestCase
{
    /**
     * @return void
     */
    public function test_google_holidays()
    {
        $response = $this->postJson('/api/working-days',
            [
                'workingHoursPerDay' => [8,8,8],
                'year' => '2023',
                'simple' => true
            ]);

        $response
            ->assertStatus(200)
            ->assertJsonCount(3)
            ->assertSimilarJson([
                "Friday" => 50,
                "Monday" => 49,
                "Thursday" => 50,
            ]);


        fwrite(STDOUT, 'Found the correct working days' . PHP_EOL);

    }
}
