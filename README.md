<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Niels Wijn

I am a PHP backend developer, full o passion and commitment to the code. Coding is a hobby and a job occupation. It's
life.

## The Challenge

The kingsCode challenge is as teh following.

### User Story

Als werknemer wil ik weten op welke weekdagen ik het beste kan werken om zo min mogelijk dagen te hoeven werken,
rekening houdend met de Nederlandse feestdagen en het aantal werkdagen in het gegeven jaar.

### API-Input

Het te berekende jaar (jaartal) en het aantal uren in de week dat door de werknemer wordt gewerkt.

### API-Output

De dagen waarop een werknemen het beste kan werken in zijn eigen voordeel.

### De klant eisen

1. De applicatie dient in Laravel opgezet te zijn.
2. Er hoeft geen frontend ontwikkeld te worden, maar het mag wel.

## Startup
1. Create an .env file from the example.
2. Add the absolute path for the database file.
3. Register for an API key with the Calender API endpoint enabled.
4. Add API key to Env file.
5. Seed the database.
6. Run Tests.
7. Add Collection to postman.


## Seeder

I Created a seeder, to prefill the database so you have some data to check

```
 php artisan migrate:fresh
 php artisan db:seed
```

## Postman

I created a Postman collection to work with the api. You can find the collection under _
postman/Challenge.postman_collection.json

### Command

I also created a command

```
php artisan kingscode:get-working-days 8 8 4 -C 5 --year=2023 -G

php artisan kingscode:get-working-days 8 8 4 --company=5 --year=2023

php artisan kingscode:get-working-days 8 8 4 -C 5 --year=2022-2030 --google

php artisan kingscode:get-working-days 8 8 4 -C 5 --year=2022-2030

php artisan kingscode:get-working-days 8 8 4 -C 5
 
```

### Testing

I added some test in

*tests/Feature*

Please run them using PHPUnit

### Google
To use the google working days, please register for an API key
https://console.cloud.google.com/project/_/google/maps-apis/credentials?_ga=2.196345134.367168987.1647295505-1925886423.1647295505  
Enable the Google Calendar APi to work with the API key.
And add the key to the .env file.
