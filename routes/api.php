<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('working-days', [Api\GoogleWorkingDaysController::class, 'index']);
Route::post('company/{company}/working-days', [Api\WorkingDaysController::class, 'index']);

Route::apiResource('company', Api\CompanyController::class);
Route::apiResource('company.holidays', Api\CompanyHolidaysController::class)
    ->only(['index', 'store', 'destroy']);
Route::apiResource('holiday', Api\HolidaysController::class)
    ->only(['destroy']);

Route::fallback(function () {
    return response()->json([
        'message' => 'Not found'
    ],
        404
    );
})->name('api.fallback');
